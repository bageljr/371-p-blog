---
date: 2022-08-027
description: "Week 8"
# featured_image: "/images/20220828_115536.jpg"
tags: []
title: "CS371p Fall 2022: Ellen Agarwal"
---

![pfp](/371-p-blog/images/20220828_115536.jpg)

# What did you do this past week?

I worked on 439, 378 symbolic programming and TREL. I started applying to internships and went home for the weekend. I spent time w/family and friends.

# What's in your way?

I need to schedule time to meet with my partner. I hate Javascript (more on that later).

# What will you do next week?

I'll work on this classes project and my other classes. I'll try to start D&D this weekend. I want to go clothes shopping and need to go grocery shopping. I also need to remember to dress for the cold weather.

# What did you think of Paper #8. The Integration Segregation Principle?

Its flawed. I hope the code blocks will render here.

```python
class Timer:
	def do_stuff(self):
		pass


class TimedDoor
	timer: Timer
	def do_stuff(self):
		...
		self.timer.do_stuff()

```

The only reason an adapter or multiple inheritance was needed was because the original author insisted on timer being an abstract class, as opposed to something you could use through composition. Once you do that, you can split out your responsibilities much better than by using an abstract class for implementations.
Multiple inheritance is alright, until you need to initialize multiple base classes or their methods otherwise conflict with each other. The example was clearly an example of bad OOP - needlessly requiring a 3rd class to be between the class and the client. The adapter isn't specific to that interface, and should be just a general object bundled with the interface.

# What was your experience of my_allocator() starter code and DigitsIterator? (this question will vary, week to week)

Haven't started on my_allocator but DigitsIterator wasn't too bad. It was a bit unclear initially, but we got the hang of it. I didn't realize it meant iterating over the digits of a number.

# What made you happy this week?

I got to spend time with my friends and family. Today, me and some friends got Kura and then we also went to the local park and ziplined and went on the swings.

# What's your pick-of-the-week or tip-of-the-week?

Javascript has an asinine import system. Actually - it has several and it's impossible to get them to work the way I want it to. The same import style can't work consistently across the project and it breaks even external modules easily. Differentiating CJS, ES6, and frontend JS is impossible. It makes it exceedingly difficult to write tests.
