---
date: 2022-08-027
description: "Week 10"
# featured_image: "/images/20220828_115536.jpg"
tags: []
title: "CS371p Fall 2022: Ellen Agarwal"
---

![pfp](/371-p-blog/images/20220828_115536.jpg)

# What did you do this past week?

I finished most of my 439 project, had parents visit and went out with friends for my birthday. I also started studying for my 439 exam and planning courses. Additionally, I slept a lot this weekend : ).

# What's in your way?

I have a 439 exam coming up which will be a hassle.

# What will you do next week?

Today, I'll get most non 439 work done and go to HEB.
I will go celebrate Halloween with some friends and study for my 439 exam.
After the exam, I want to go shopping with a friend - I need more clothes and shoes and such.

# What did you think of Paper 10. Why getter and setter methods are evil?

It has a pretty good point. They break down the procedural boundary between objects.

# What was your experience of iteration and initializations? (this question will vary, week to week)

Iteration and initializations made sense.

# What made you happy this week?

Family visiting and going out with friends was really fun. I'm excited for Halloween.

# What's your pick-of-the-week or tip-of-the-week?

Tailscale - a new kind of VPN. Its based off wireguard and essentially configures it for you. Unlike vanilla wireguard, its NAT traversal and relays saves you from opening ports, greatly increasing security. Unlike a traditional VPN, it routes your traffic directly to other devices, not through a central server, which is only used for the setup process. In some edge cases, they use relays to get around firewalls, but they try to reduce that since it costs them money. Therefore, they cannot observe your network traffic like a traditional VPN.
Its free for most personal uses and much easier to use there. They're constantly developing new and interesting features - such as a full WASM SSH+Tailscale client. This can eventually be a serious competitor to both traditional networking systems (firewalls, VPNs, etc) and also systems like CITRIX that provide remote access to services.
On a personal level, it acts both as a unifying network for your devices (such as KDEconnect) and also a personal VPN (with a raspberry pi). For example, we used it as an exit node in India to hide our traffic. Since you're running the exit node, its harder to block and the only person who can monitor it is you (and your ISP).
If you're concerned about the control server, a FOSS version exists (Headscale), but it does loose some of the benefits of Tailscale.
