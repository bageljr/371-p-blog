---
date: 2022-08-027
description: "Week 2"
# featured_image: "/images/20220828_115536.jpg"
tags: []
title: "CS371p Fall 2022: Ellen Agarwal"
---

![](/images/20220828_115536.jpg)

## What did you do this past week?

I made a meta array for project 1. I was inspired by Daniel's idea.

## What's in your way?

I haven't checked Hackerrank's memory limit or compile time limit. Also, even if each Collatz is solved in O(1), looking it up will be O(N) - we're trying to get the max of an array.

## What will you do next week?

Implement further compile-time optimizations on my meta-array.

## What did you think of Paper #1: Syllabus?

I don't like Persuall but the spec grading makes it work.

## What was your experience of assertions, unit tests, and coverage? (this question will vary, week to week)

I haven't tried it in C++ yet. However, I like it less than the python version - my editor can do pytest very well, but it doesn't apply to google test. Using a header for it feels wrong.

## What made you happy this week?

I accomplished several personal goals and got into TREL.

## What's your pick-of-the-week or tip-of-the-week?

LanguageTool - a FOSS version of Grammarly. I use it in my editor (Neo vim) via ltex-ls. Of course, I ignore most of its suggestions, but there's only so much it can do.
