---
date: 2022-08-027
description: "Week 14"
# featured_image: "/images/20220828_115536.jpg"
tags: []
title: "CS371p Fall 2022: Ellen Agarwal: Final Entry"
---

![pfp](/371-p-blog/images/20220828_115536.jpg)

# How well do you think the course conveyed those takeaways?

I think the course helped somewhat. I've learned some of this before, but this course was helpful. The ideas given are very important for good software development.

# Were there any other particular takeaways for you?

A lot of the specifics of C++ and OOP like L and R values. Some projects actually helped learning more complicated OOP designs.

# How did you feel about cold calling?

It caught me by surprise on the first time but overall it's not a bad idea. However, despite me normally being an enthusiastic member of CS class, cold calling does make me more nervous to answer questions in class.

# How did you feel about specifications grading?

I really like this classes system for grading by spec. The only issue is that there are only 5 project points, and you need 5 to get an A. That way if you mess up a single project and aren't able to correct it you can't fix it.

# How did you feel about help sessions and office hours?

I didn't really attend them.

# How did you feel about the support from the TAs?

They were helpful.

# You should have read five papers that describe SOLID design: Single Responsibility, Open-Closed Principle, Liskov Substitution, Interface Segregation, and Dependency Inversion. What insights have they given you?

These are good ideas, though the papers were a drag to read. Encapsulation and interfaces solves these problems better IMO.

# You should have read two papers that advised minimizing getters and setters. What insights have they given you?

They are correct but avoiding them completely is difficult and unnecessary.

# What required tool did you not know and now find very useful?

nix-collect-garbage. Without it, my nix cache overflowed the UT CS storage. I'm surprised it isn't enabled by default.

# In the end, how much did you learn relative to other UT CS classes?

I learned a good amount. While I knew some C++ at UNT over 3 courses, I feel like I learned more in this class. It did feel slow, especially in the first half of the class - we spend a lot of time going through the cold-calling process, making it a lot slower.
