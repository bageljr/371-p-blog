---
date: 2022-08-027
description: "Week 1"
# featured_image: "/images/20220828_115536.jpg"
tags: []
title: "Week 1 setting up a blog."
---

![](/images/20220828_115536.jpg)
I grew up in California - till 2nd grade, where I moved to Allen, TX.
For high school, I went to Allen High School for 9th and 10th grade and then Texas Academy of Math and Science for the last two years of high school.
My favorite extracurricular was robotics.
I loved building robots and working on a team.
I came to UT because I wanted to study CS - which I like because I like working with computers.
I'm in this class because I want to improve my Object-Oriented programming skills.

I took 3 semesters of C++ at UNT, so while I have some experience, it isn't very good, and I'm rusty.
Hopefully, this class will fill me in on some of the more modern features of C++ I didn't learn at UNT.

While I liked the first few lectures, cold-calling caught me by surprise that first time.
After all, I was the first person to get cold called.
Specifications grading seems like a good system, especially since I took 104c last week, and it was a big part of why I'm in this course.
This week I'm happy to be meeting all my friends again.

Pick of the week:

- Hugo, the static site generator I'm using for this blog. Its working better than pagic.
