---
date: 2022-08-027
description: "Week 4"
# featured_image: "/images/20220828_115536.jpg"
tags: []
title: "CS371p Fall 2022: Ellen Agarwal"
---

![pfp](/371-p-blog/images/20220828_115536.jpg)

# What did you do this past week?

Emma and I implemented most of the program using a tree. When we read the ballots, we read them into a tree. Since each ballot is irrelevant, we sum them up - meaning duplicate ballots don't slow down our program much. Each round, we'll remove the lowest ballot(s) and merge the removed nodes into the rest of the tree. For example, if we eliminate 3, and it has a child of 2, we'll merge it with the 2 on the same level as the removed 3, transferring the votes. Finally, we'll purge the lowest ballot(s) from the remaining sub-trees. Its very fast but still fails some edge cases with ties and zero votes. When running it in debug mode, its very satisfying to watch it quickly reduce a tree. We need to ensure our purge function works.

# What's in your way?

Our program doesn't handle ties correctly yet. C++ is very stupid in how it handles incomplete types, forcing us to use a vector. I need to work on stuff for other classes. I also have a 439 exam this week.

# What will you do next week?

Monday evening we'll try to finish the hackerrank portion of the project. I want to be among the first to get all 3/3.

# What did you think of Paper #5: The Single Responsibility Principle?

Not the biggest fan, it was short and repetitive. I wish we pulled articles from hackernews or similar current sources.

# What was your experience of arguments and const? (this question will vary, week to week)

I feel like this week was very slow. The water gun example was weird and awkward. While I appreciate the focus on the fundamentals, I feel like it should be faster.

# What made you happy this week?

I just went to Mozart's with some friends! The coffee and croissant was OK, but the experience was great.

# What's your pick-of-the-week or tip-of-the-week?

Nix - a package manager which does alot of very interesting things to ensure reproducible and isolated packages. It works for rootless package management, but requires some hacks. It's much faster and more reliable than homebrew, but I prefer pacman when available. Its declarative management system has issues and is far too complicated. I wish someone made a simpler way to manage this which worked rootless as well.
