---
date: 2022-08-027
description: "Week 3"
# featured_image: "/images/20220828_115536.jpg"
tags: []
title: "CS371p Fall 2022: Ellen Agarwal"
---

![pfp](/371-p-blog/images/20220828_115536.jpg)

# What did you do this past week?

I worked a lot on my projects for symbolic programming and OS. I also made my meta-array solution slightly faster. I also realized it probably is hitting the maximum unsigned int size. Therefore, I updated the maximum limit but that hasn't fully worked yet.

# What's in your way?

I don't know if the meta array works during compile time right now or if the memory limit is too large. It's too slow to pass some tests. I'm guessing its either slow at reading it from disk or more likely, calculating when the array is loaded into the program memory, not at compile time - defeating the whole purpose.

# What will you do next week?

Ideally, I'll finish project 1 today and I'll need to find a partner for the next one, or do it myslef. I also am going to start working on my projects for TREL, which likely involves learning JavaScript to manage the frontend. I'm very excited to work with them!

# What did you think of Paper #3: Continuous Integration?

It's a very old paper. It doesn't reflect the extremely high usage of git, dominance of CI systems that are built into your VCS host like github and gitlab or fully reproducible builds like earthly. However, many of its points are accurate, since they reflected my experience implementing and using CI.

# What was your experience of exceptions, char\*, std::string, and ==? (this question will vary, week to week)

Exceptions are a lot like python exceptions, so they make sense. I didn't realize C++'s raw strings were still char\*, it seems counterintuitive. std::string makes a lot more sense in some ways but is way worse in others. Unicode support is important in alot of practical applications while single-byte arrays are much more useful in theoretical or academic problems. Operator overloading is very powerful and pretty intuitive. However, I like how this course goes into the tricky things like R and L values, unlike my past C++ course.

# What made you happy this week?

I got to hang out with friends and my very stupid solution passed tests one and two on hackerrank.

# What's your pick-of-the-week or tip-of-the-week?

My pick of the week is neovim because I lack originality. I've gotten a lot better at using it, and it has a huge open source community. At its core, its vim - so many of its features can be used even in vi. But at the same, with TS/LSP/DAP it can punch way above its weight. Unlike VScode, its not electron based and therefore much more lightweight.
