---
date: 2022-08-027
description: "Week 9"
# featured_image: "/images/20220828_115536.jpg"
tags: []
title: "CS371p Fall 2022: Ellen Agarwal"
---

![pfp](/371-p-blog/images/20220828_115536.jpg)

# What did you do this past week?

I worked on 439 and played hollow knight. This Saturday, I went out with some friends to get Halloween costumes and just hung out and played games. It was very fun. I also started on Allocator with my partner.

# What's in your way?

We aren't as far on allocator as I'd like, and I'm overall a bit behind. 439 still takes up much of my time.

# What will you do next week?

I'll celebrate my birthday with friends and family and try not do die from all the work meanwhile. I need to finish Allocator, 1-2 symbollic projects, a 439 project, and a REP project.

# What did you think of Paper #9. The Dependency Inversion Principle?

I am beginning to dislike these papers in general. Next week, I'll use GPT3. You can get it via [openai](https://beta.openai.com/playground). We asked Downing about it earlier in class and he said it was ok - but if it isn't I won't do it. I hope we can copy text from perusal directly, otherwise we'll need image to text.

# What was your experience of Stack, explicit, and friend? (this question will vary, week to week)

Stack was cool, explicit/friend makes sense. I enjoy learning C/C++ correctly. I'm still struggling to grasp how lacking bindings for templates has changed C++'s typing compared to pythons. For example - ostream's friend >> operator returns an ostream, not the passed type - so it'd discard inherited types in the system, while python would allow you to do that pretty easily.

# What made you happy this week?

I got to hang out with friends this weekend. I got a Halloween costume I'm excited to wear and played magic the gathering and The Mind (The Mind is a psychopathic game designed by psychopaths. It's the card game equivalent of a horror movie. You and 1-3 others have to put cards 1-100 in ascending order without communicating. Each level you each get X cards where X is the level number. But we beat level 8!).

# What's your pick-of-the-week or tip-of-the-week?

My pick of the week is GPT3 - perfectly allowed in many courses to do your homework for you. I can't wait till most homework assignments are written, completed, and graded by AI. It'll be amazing!
