---
date: 2022-08-027
description: "Week 4"
# featured_image: "/images/20220828_115536.jpg"
tags: []
title: "CS371p Fall 2022: Ellen Agarwal"
---

![pfp](/371-p-blog/images/20220828_115536.jpg)

# What did you do this past week?

I worked on my 439 project. I submitted the final version for this course, but I
had to use the intended solution instead of my meta array. I clearly put too
much effort into the meta array.

# What's in your way?

I should find a partner for my project in this class. I know a couple people, so
I may reach out.

# What will you do next week?

First, I will finish my project for 439.
Second, I will finish unit tests for the TREL MCS backend.
Third, I will find a partner for this class or just start the project.
Fourth, I'll start applying to companies for a summer internship.

# What did you think of Paper #4: Pair Programming?

I absolutely hated it. We've gone through the same paper 3 times now - 314, 439 and now this.
It was interesting back in 314. In 439, I was resigned. Here I'm just done with annotating the same paper repeatedly.
Even reading it for the first time, it felt very preachy and as a result harder to internalize.
I'd much prefer a more balanced "Should we pair program?" or a "here's how to pair program".
Instead, we get "Pair programming is good for you, here's how to do it".
But since we can see our persuall grades ahead of time, I feel slightly better about it here.

# What was your experience of StrCmp, pointers, references? (this question will vary, week to week)

I've done this before back in UNT. Obviously pointers makes sense now,
especially with 429 experience to back it up. However, I used to expect
references to just work and never questioned how they did or the actual
semantics of it. Learning L values and R values really helps with them. I still
have the question:

```cpp
int* ptr_array = malloc(sizeof(int) * 1);
int& ref = ptr_array[0];
ptr_array++;
```

Since we can't know ptr_array ahead of time, and it can change independently of
ref, does that mean ref must occupy 8 bytes of stack memory to store a refence?

# What made you happy this week?

I'm happy that my father came over this weekend. I also got to play at Texas
Tabletop and we played Secret Hitler, Spyfall, Splendor and love letter.
TREL is also starting up so that's fun.

# What's your pick-of-the-week or tip-of-the-week?

My pick of the week is topgrade - a tool to run a bunch of package manager
updates at the same time. I'll probably use it instead of upgrading each one
manually or my own script for the same.
