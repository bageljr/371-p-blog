---
date: 2022-08-027
description: "Week 12"
# featured_image: "/images/20220828_115536.jpg"
tags: []
title: "CS371p Fall 2022: Ellen Agarwal"
---

![pfp](/371-p-blog/images/20220828_115536.jpg)

# What did you do this past week?

I worked on my project for 439 a little, played a lot of Rimworld. I also did a lot of work for this classes project - Darwin. I hung out with friends this weekend and got new shoes. I also played at TTT - probably for the last real full event.

# What's in your way?

Finals, a shrinking job market, and ~~my mortality~~ my remaining projects. Mainly Operating systems.

# What will you do next week?

I'll try to finish projects for this class and 439.

# What did you think of Paper #12. Why extends is evil?

I really like the concept but dislike papers in general - I feel like studying the Zen of Python would be more fun.

# What was your experience of function overloading, move, and vector? (this question will vary, week to week)

Overloading is cool and useful. Move is something I didn't know at all before and explains a huge missing hole in my C++ knowledge. Vector is interesting and I kinda like the frequent exercises. However - you can pass all the test cases by commenting out their #DEFINE since the switch contains no default fail case.

# What made you happy this week?

What made me happy was my friends as usual, we played magic and got food and such. Also Texas Tabletop was very fun as usual. I'm looking forward to the breaks but also dread parting from my friends who're in Austin.

# What's your pick-of-the-week or tip-of-the-week?

My pick of the week is BTDU - the best disk usage analyzer I've seen. Say your disk is full, and you want to find what occupies space. A traditional disk analyzer would recursively iterate over all directories and files to find what uses your space. This is very slow and frustrating. But you don't care about small files and folders- only big ones. So what BTDU does is it samples your file system repeatedly, giving you a more accurate view of your file system over time. While its initial view is very inaccurate, it gets very good very quickly and tells you what uses your space much faster than traditional applications. Unfortunately, it only works on BTRFS on Linux and uses your whole CPU.
