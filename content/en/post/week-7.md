---
date: 2022-08-027
description: "Week 7"
# featured_image: "/images/20220828_115536.jpg"
tags: []
title: "CS371p Fall 2022: Ellen Agarwal"
---

![pfp](/371-p-blog/images/20220828_115536.jpg)

# What did you do this past week?

My partner and I finished project 2 - voting and submitted it. I've been working on my 439 project a lot and also been working on the mission control server for TREL. I also hung out with friends a good bit.

# What's in your way?

439 occupies a lot of time and energy. We had an exam and I have to regrade and look through my stuff. I don't think the details for project 3 are out yet, but I know I'm working with John.

# What will you do next week?

I will start on project 3 if available. As usual, I'll work on my projects for other classes too. I will also start applying for internships. I also need to do a bunch of other stuff for 439. I'm also going home for the weekend and get to see some friends I haven't in a while.

# What did you think of Paper #7: The Liskov Substitution Principle?

I thought it was cool to learn something that explains warnings I got in python. It makes sense, but it's a reason I'd prefer interfaces to objects. It was very long and could really use color (for the examples) or something similar to be more engaging.

# What was your experience of std::array() and std::vector? (this question will vary, week to week)

I feel like I understand these a bit better and the course is coming together. I'm really excited about the next parts of this course.

# What made you happy this week?

Yesterday, I hung out with some friends, and we went to the mall and played a lot of Magic the Gathering. It was very fun, and it made me much happier. I got two nice shirts and some really cool dice. We also played magic the gathering and my decks did pretty well. We also met up for Texas Tabletop which was fun and got cookies.

# What's your pick-of-the-week or tip-of-the-week?

My pick of the week is catppuccin. I really like the theme and I find it really cool how the maintainer implemented compiling of the theme. It also has integrations for a lot of different neovim plugins and a good theme for wezterm. I still kinda miss tokyonight.nvim.
