---
date: 2022-08-027
description: "Week 11"
# featured_image: "/images/20220828_115536.jpg"
tags: []
title: "CS371p Fall 2022: Ellen Agarwal"
---

![pfp](/371-p-blog/images/20220828_115536.jpg)

# What did you do this past week?

I studied heavily for my 439 exam and kinda conked out after that. I did hang out with friends a bunch and went shopping today.

# What's in your way?

A shrinking CS job market.

# What will you do next week?

I will work on projects for this class (Darwin), REP/TREL, and 439. I will also apply to more internships and try to get some other things started. I'll also try to get new shoes and glasses.

# What did you think of Paper 11. More on getters and setters?

I still dislike these papers.

# What was your experience of initializer_list and vector? (this question will vary, week to week)

The two successive exercises was a bit surprising, but I kinda like this so far. I'm beginning to understand that this is not just a C++ course but a course focused on the fundamentals of Object-Oriented Programming.

# What made you happy this week?

I went shopping with friends and otherwise hung out with others. Spy X Family is amazing.

# What's your pick-of-the-week or tip-of-the-week?

My pick of the week is the lawsuit against GitHub copilot. This has been brewing for a while but its likely going to be a landmark in AI interactions with copyright. Simply searching a GitHub repo for code and copying it without following the license is illegal. However, can an overfitted AI do the same? Would this apply to other media - like pictures or even entire movies? What are the limitations of these AI models? If its ruled as not fair use, will github do so under its own TOS rather than fair use. Personally, I think that it is not fair use and that GitHub needs to be legally responsible for its products. Furthermore, a ruling fully in favor of GitHub would be disastrous for other industries - imagine if I could train an AI on music and have it produce verbatim Taylor Swift songs and resell them. If training AI is fair use, it applies to all copyrighted material. An overfitted AI would be able to essentially strip copyright from works it sources, as is somewhat the case with Copilot. The real interesting rulings are down the middle - such as if Copilot is legal only for one line at a time.
