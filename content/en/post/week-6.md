---
date: 2022-08-027
description: "Week 4"
# featured_image: "/images/20220828_115536.jpg"
tags: []
title: "CS371p Fall 2022: Ellen Agarwal"
---

![pfp](/371-p-blog/images/20220828_115536.jpg)

# What did you do this past week?

My partner and I finished our project for this class. I've been also doing a 439 exam and working on stuff for TREL and Symbolic programming. Besides that, I hung out with friends and played hollow knight. Hollow knight is pretty fun.

# What's in your way?

In this class, not much. OS consumes a lot of time and energy. Also its grading system is frustrating, especially when compared to this class's similar one.

# What will you do next week?

I need to finish stuff for TREL and OS. I also will hang out with friends more and try to complete several pending items. This weekend, I'm starting a D&D campaign.

# What did you think of Paper #6: The Open-Closed Principle?

I think it's a cool idea and explains a lot. It makes me question some of my own decisions in programming, both in internship and FOSS work, where others are responsible for maintaining my code. I think that composition is better than inheritance and it solves this problem much more nicely. I think if python had stronger interfaces w/ABC or implicit protocols it'd be a great example here. Otherwise, you can look to Rust's OOP system, which I think only does interfaces in the form of traits.

# What was your experience of equals(), templates, and iterators? (this question will vary, week to week)

I feel like this class is finally picking up, and I'm excited. Templates are very cool, they remind me a lot of python generics, a new feature to its language. I'm surprised that they don't let you enforce base types like python's do, since that seems like an obvious use case. It explains why C++ compilation errors are so hard to debug - it doesn't handle typing in any of the underlying functions. If anything, I'd expect python to rely on dynamic systems and C++ to rely on static types. Iterators are cool, but I still like python's implementation more with its yield function simplifying this greatly. I feel like python has learned a lot from C++ and its interesting going back to C++ after so long in python land.

# What made you happy this week?

I got to hang out with some friends this Friday and tomorrow. Also, SIX SEASONS AND A MOVIE! IT'S HAPPENING! THE COMMUNITY MOVIE IS COMING!

# What's your pick-of-the-week or tip-of-the-week?

My pick of the week is Home Assistant, the best home automation system I've seen.
